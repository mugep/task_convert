import { Sjk } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";

export class SubscribeToSJKService {
    private static subscribeToSjkRepository: Repository<Sjk> = null;

    constructor(di: IDI) {
        if (SubscribeToSJKService.subscribeToSjkRepository === null) {
            SubscribeToSJKService.subscribeToSjkRepository = di.appTypeOrm.getConnection().getRepository(Sjk);
        }
    }

    public async findAll(query: PageableGetParam): Promise<Sjk[]> {
        return await SubscribeToSJKService.subscribeToSjkRepository.find(Pagination.paginateOption(query));
    }

    public async findById(id: number): Promise<Sjk> {
        try {
            return await SubscribeToSJKService.subscribeToSjkRepository
                .findOne({
                    where: {
                        id
                    }
                });
        } catch (e) {
            console.log(e);
            throw new DataNotFoundError("Invalid code");
        }
    }

    public async create(data: Sjk): Promise<Sjk> {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const subcribeToSJK = new Sjk();
        subcribeToSJK.name = data.name;
        subcribeToSJK.email = data.email;
        subcribeToSJK.sjkno = data.sjkno;
        subcribeToSJK.base = data.base;
        subcribeToSJK.issuedDate = data.issuedDate;
        try {
            return await SubscribeToSJKService.subscribeToSjkRepository.save(subcribeToSJK);
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }

    public async update(data: Sjk): Promise<Sjk> {
        const subcribeToSJK: Sjk = await this.findById(data.id);
        if (subcribeToSJK == null) {
            throw new DataNotFoundError(`SJK not found: ${data.id}`);
        }

        if (data.name !== null || data.name !== undefined) {
            subcribeToSJK.name = data.name;
        }

        return await SubscribeToSJKService.subscribeToSjkRepository.save(subcribeToSJK);
    }

    public async delete(code: number): Promise<Sjk> {
        const subcribeToSJK: Sjk = await this.findById(code);
        if (subcribeToSJK == null) {
            throw new DataNotFoundError(`SJk not found: ${code}`);
        }
        await SubscribeToSJKService.subscribeToSjkRepository.delete(subcribeToSJK);
        return subcribeToSJK;
    }
}