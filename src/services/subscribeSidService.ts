import { Sid } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";

export class SubscribeToSIDService {
    private static subscribeToSidRepository: Repository<Sid> = null;

    constructor(di: IDI) {
        if (SubscribeToSIDService.subscribeToSidRepository === null) {
            SubscribeToSIDService.subscribeToSidRepository = di.appTypeOrm.getConnection().getRepository(Sid);
        }
    }

    public async findAll(query: PageableGetParam): Promise<Sid[]> {
        return await SubscribeToSIDService.subscribeToSidRepository.find(Pagination.paginateOption(query));
    }

    public async findById(id: number): Promise<Sid> {
        try {
            return await SubscribeToSIDService.subscribeToSidRepository
                .findOne({
                    where: {
                        id
                    }
                });
        } catch (e) {
            console.log(e);
            throw new DataNotFoundError("Invalid code");
        }
    }

    public async create(data: Sid): Promise<Sid> {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const subcribeToSID = new Sid();
        subcribeToSID.name = data.name;
        subcribeToSID.email = data.email;
        subcribeToSID.sidno = data.sidno;
        subcribeToSID.issuedDate = data.issuedDate;
        try {
            return await SubscribeToSIDService.subscribeToSidRepository.save(subcribeToSID);
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }

    public async update(data: Sid): Promise<Sid> {
        const subcribeToSID: Sid = await this.findById(data.id);
        if (subcribeToSID == null) {
            throw new DataNotFoundError(`SID not found: ${data.id}`);
        }

        if (data.name !== null || data.name !== undefined) {
            subcribeToSID.name = data.name;
        }

        return await SubscribeToSIDService.subscribeToSidRepository.save(subcribeToSID);
    }

    public async delete(code: number): Promise<Sid> {
        const subcribeToSID: Sid = await this.findById(code);
        if (subcribeToSID == null) {
            throw new DataNotFoundError(`SJk not found: ${code}`);
        }
        await SubscribeToSIDService.subscribeToSidRepository.delete(subcribeToSID);
        return subcribeToSID;
    }
}