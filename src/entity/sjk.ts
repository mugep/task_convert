import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({
    name: 'sjk'
})

export class Sjk {
    @PrimaryGeneratedColumn({name: 'id'})
    id: number;

    @Column({name: 'name'})
    name: string;

    @Column({name: 'email'})
    email: string;

    @Column({name: 'sjkNo'})
    sjkno: number;

    @Column({name: 'base'})
    base: string;

    @Column({name: 'issuedDate'})
    issuedDate: Date;
}