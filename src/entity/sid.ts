import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({
    name: 'sid'
})

export class Sid {
    @PrimaryGeneratedColumn({name: 'id'})
    id: number;

    @Column({name: 'name'})
    name: string;

    @Column({name: 'email'})
    email: string;

    @Column({name: 'sidNo'})
    sidno: number;

    @Column({name: 'issuedDate'})
    issuedDate: Date;
}