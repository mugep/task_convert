import { Validator } from "le-validator";
import { PartialResponsify } from "partial-responsify";
import { Sid, Sjk } from "../entity";
import { IDI } from "../interface";
import {
    Config,
    Helper,
    AppTypeOrm
} from "../lib";
import { SubscribeToSIDService, SubscribeToSJKService } from "../services";

export const getAppTypeOrm = async (config: Config): Promise<AppTypeOrm> => {
    return new AppTypeOrm(await AppTypeOrm.getConnectionManager({
        database: config.dbDatabase,
        entities: [
            Sjk,
            Sid
        ],
        host: config.dbHost,
        logging: config.dbLogging,
        password: config.dbPassword,
        type: config.dbType,
        username: config.dbUsername,
    }));
};

export const getDiComponent = async (config: Config): Promise<IDI> => {
    const di: IDI = {
        appTypeOrm: await getAppTypeOrm(config),
        config,
        helper: new Helper(),
        partialResponsify: new PartialResponsify(),
        validator: new Validator(),
        subscribeSjkService: null,
        subscribeSidService: null
    };
    di.subscribeSjkService = new SubscribeToSJKService(di);
    di.subscribeSidService = new SubscribeToSIDService(di);
    return di;
};
