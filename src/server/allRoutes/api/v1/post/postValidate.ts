import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../interface";
import { ValidatorGetParam } from "../../../../../lib";
// import {
//     commonHeaderParams,
// } from "../../../../../scripts";
import {PageableGetParam} from "../../../../../lib/appValidator";

const path = "/v1/validate";
const method = "POST";
const get: ValidatorGetParam[] = [];
// const body: BodyFormat = {
//     required: true,
//     type: "object",
//     swagger: {
//         description: "",
//         example: {
//             name: "",
//             email: "",
//             sjkNo: "",
//             base: "",
//             issuedDate: ""
//         },
//     },
//     fields: {
        
//     },
// };
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
    items: {
        fields: {
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat: ResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx: IRouterContext): Promise<void> => {
    const di: IDI = ctx.state.di;

    // Validate query params
    di.validator.processQuery<PageableGetParam>(get, ctx.request.query);

    ctx.status = 200;
    ctx.body = "OK";
};
export const openapiPostValidate: IOpenApiRoute = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
export const postValidate: IAllRoute = {
    func,
    method,
    path,
};
