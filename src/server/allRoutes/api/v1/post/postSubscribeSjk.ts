import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../interface";
import { BodyFormat, ValidatorGetParam } from "../../../../../lib";
// import {
//     commonHeaderParams,
// } from "../../../../../scripts";
import {PageableGetParam} from "../../../../../lib/appValidator";
import { Sjk, Sid } from "../../../../../entity";

const path = "/v1/sjk/subscribe";
const method = "POST";
const get: ValidatorGetParam[] = [];
const body: BodyFormat = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            name: "",
            email: "",
            sjkNo: "",
            base: "",
            issuedDate: ""
        },
    },
    fields: {
        name: {
            type: "string",
            required: true
        },
        email: {
            type: "string",
            required: true
        },
        sjkno: {
            type: "number",
            required: false
        },
        sidno: {
            type: "number",
            required: false
        },
        base: {
            type: "string",
            required: false
        },
        issuedDate: {
            type: "string",
            required: true
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            name: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat: ResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx: IRouterContext): Promise<void> => {
    const di: IDI = ctx.state.di;

    // Validate query params
    di.validator.processQuery<PageableGetParam>(get, ctx.request.query);


    // Validate body
    const requestBody = di.validator.processBody<Sjk & Sid>(body, ctx.request.body);
    console.log('tipe', requestBody);
    
    if(requestBody.sjkno) {
        ctx.body = await di.subscribeSjkService.create(requestBody);
    } else if(requestBody.sidno) {
        ctx.body = await di.subscribeSidService.create(requestBody);
    }
    console.log(requestBody);
    
    // const requestBody = di.validator.processBody<Sjk>(body, ctx.request.body);

    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);

    ctx.status = 200;
    // ctx.body = subscribeService;
};
export const openapiPostSubscribeSjk: IOpenApiRoute = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
export const postSubscribeSjk: IAllRoute = {
    func,
    method,
    path,
};
