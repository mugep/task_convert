"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDiComponent = exports.getAppTypeOrm = void 0;
const le_validator_1 = require("le-validator");
const partial_responsify_1 = require("partial-responsify");
const entity_1 = require("../entity");
const lib_1 = require("../lib");
const services_1 = require("../services");
exports.getAppTypeOrm = async (config) => {
    return new lib_1.AppTypeOrm(await lib_1.AppTypeOrm.getConnectionManager({
        database: config.dbDatabase,
        entities: [
            entity_1.Sjk,
            entity_1.Sid
        ],
        host: config.dbHost,
        logging: config.dbLogging,
        password: config.dbPassword,
        type: config.dbType,
        username: config.dbUsername,
    }));
};
exports.getDiComponent = async (config) => {
    const di = {
        appTypeOrm: await exports.getAppTypeOrm(config),
        config,
        helper: new lib_1.Helper(),
        partialResponsify: new partial_responsify_1.PartialResponsify(),
        validator: new le_validator_1.Validator(),
        subscribeSjkService: null,
        subscribeSidService: null
    };
    di.subscribeSjkService = new services_1.SubscribeToSJKService(di);
    di.subscribeSidService = new services_1.SubscribeToSIDService(di);
    return di;
};
//# sourceMappingURL=index.js.map