"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscribeToSIDService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class SubscribeToSIDService {
    constructor(di) {
        if (SubscribeToSIDService.subscribeToSidRepository === null) {
            SubscribeToSIDService.subscribeToSidRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Sid);
        }
    }
    async findAll(query) {
        return await SubscribeToSIDService.subscribeToSidRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await SubscribeToSIDService.subscribeToSidRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const subcribeToSID = new entity_1.Sid();
        subcribeToSID.name = data.name;
        subcribeToSID.email = data.email;
        subcribeToSID.sidno = data.sidno;
        subcribeToSID.issuedDate = data.issuedDate;
        try {
            return await SubscribeToSIDService.subscribeToSidRepository.save(subcribeToSID);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const subcribeToSID = await this.findById(data.id);
        if (subcribeToSID == null) {
            throw new error_1.DataNotFoundError(`SID not found: ${data.id}`);
        }
        if (data.name !== null || data.name !== undefined) {
            subcribeToSID.name = data.name;
        }
        return await SubscribeToSIDService.subscribeToSidRepository.save(subcribeToSID);
    }
    async delete(code) {
        const subcribeToSID = await this.findById(code);
        if (subcribeToSID == null) {
            throw new error_1.DataNotFoundError(`SJk not found: ${code}`);
        }
        await SubscribeToSIDService.subscribeToSidRepository.delete(subcribeToSID);
        return subcribeToSID;
    }
}
exports.SubscribeToSIDService = SubscribeToSIDService;
SubscribeToSIDService.subscribeToSidRepository = null;
//# sourceMappingURL=subscribeSidService.js.map