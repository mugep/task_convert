"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class SupplierService {
    constructor(di) {
        if (SupplierService.supplierRepository === null) {
            SupplierService.supplierRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Supplier);
        }
    }
    async findAll(query) {
        return await SupplierService.supplierRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await SupplierService.supplierRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const supplier = new entity_1.Supplier();
        supplier.name = data.name;
        try {
            return await SupplierService.supplierRepository.save(supplier);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(newSupplier) {
        const supplier = await this.findById(newSupplier.id);
        if (supplier == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${newSupplier.id}`);
        }
        if (newSupplier.name !== null || newSupplier.name !== undefined) {
            supplier.name = newSupplier.name;
        }
        return await SupplierService.supplierRepository.save(supplier);
    }
    async delete(code) {
        const supplier = await this.findById(code);
        if (supplier == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        await SupplierService.supplierRepository.delete(supplier);
        return supplier;
    }
}
exports.SupplierService = SupplierService;
SupplierService.supplierRepository = null;
//# sourceMappingURL=supplierService.js.map