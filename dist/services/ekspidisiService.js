"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EkspidisiService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class EkspidisiService {
    constructor(di) {
        if (EkspidisiService.ekspidisiRepository === null) {
            EkspidisiService.ekspidisiRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Ekspidisi);
        }
    }
    async findAll(query) {
        return await EkspidisiService.ekspidisiRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['destinationCity', 'originCity']
        }));
    }
    async findById(id) {
        try {
            return await EkspidisiService.ekspidisiRepository
                .findOne({
                where: {
                    id
                },
                relations: ['destinationCity', 'originCity']
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const ekspidisi = new entity_1.Ekspidisi();
        ekspidisi.tanggal = data.tanggal;
        ekspidisi.originCity = data.originCity;
        ekspidisi.destinationCity = data.destinationCity;
        try {
            return await EkspidisiService.ekspidisiRepository.save(ekspidisi);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(newEkspidisi) {
        const ekspidisi = await this.findById(newEkspidisi.id);
        if (ekspidisi == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${newEkspidisi.id}`);
        }
        if (newEkspidisi.originCity !== null || newEkspidisi.originCity !== undefined) {
            ekspidisi.originCity = newEkspidisi.originCity;
        }
        if (newEkspidisi.destinationCity !== null || newEkspidisi.destinationCity !== undefined) {
            ekspidisi.destinationCity = newEkspidisi.destinationCity;
        }
        return await EkspidisiService.ekspidisiRepository.save(ekspidisi);
    }
    async delete(code) {
        const ekspidisi = await this.findById(code);
        if (ekspidisi == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        await EkspidisiService.ekspidisiRepository.delete(ekspidisi);
        return ekspidisi;
    }
}
exports.EkspidisiService = EkspidisiService;
EkspidisiService.ekspidisiRepository = null;
//# sourceMappingURL=ekspidisiService.js.map