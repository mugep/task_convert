"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class PostService {
    constructor(di) {
        if (PostService.postRepository === null) {
            PostService.postRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Post);
        }
    }
    async findAll(query) {
        return await PostService.postRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(code) {
        try {
            return await PostService.postRepository
                .findOne({
                where: {
                    code,
                },
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const post = new entity_1.Post();
        post.author = data.author;
        post.description = data.description;
        post.createdAt = new Date();
        post.updatedAt = new Date();
        try {
            return await PostService.postRepository.save(post);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(newPost) {
        const post = await this.findById(newPost.code);
        if (post == null) {
            throw new error_1.DataNotFoundError(`Post not found: ${newPost.code}`);
        }
        if (newPost.description !== null || newPost.description !== undefined) {
            post.description = newPost.description;
        }
        return await PostService.postRepository.save(post);
    }
    async delete(code) {
        // const post: Post = await this.findById(code);
        // if (post == null) {
        //     throw new DataNotFoundError(`Post not found: ${code}`);
        // }
        // await PostService.postRepository.delete(post);
        // return post;
        const post = await this.findById(code);
        await PostService.postRepository.delete(code);
        return post;
    }
}
exports.PostService = PostService;
PostService.postRepository = null;
//# sourceMappingURL=postService.js.map