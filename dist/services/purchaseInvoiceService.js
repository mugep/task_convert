"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseInvoiceService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const productService_1 = require("./productService");
const purchaseOrderService_1 = require("./purchaseOrderService");
const purchaseInvoiceDetailService_1 = require("./purchaseInvoiceDetailService");
class PurchaseInvoiceService {
    constructor(di) {
        if (PurchaseInvoiceService.purchaseInvoiceRepository === null) {
            PurchaseInvoiceService.purchaseInvoiceRepository = di.appTypeOrm.getConnection().getRepository(entity_1.PurchaseInvoice);
        }
        if (PurchaseInvoiceService.purchaseOrderService === null) {
            PurchaseInvoiceService.purchaseOrderService = new purchaseOrderService_1.PurchaseOrderService(di);
        }
        if (PurchaseInvoiceService.purchaseInvoiceDetailService === null) {
            PurchaseInvoiceService.purchaseInvoiceDetailService = new purchaseInvoiceDetailService_1.PurchaseInvoiceDetailService(di);
        }
        if (PurchaseInvoiceService.productService === null) {
            PurchaseInvoiceService.productService = new productService_1.ProductService(di);
        }
    }
    async findAll(query) {
        return await PurchaseInvoiceService.purchaseInvoiceRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['purchaseOrder']
        }));
    }
    async findById(id) {
        try {
            return await PurchaseInvoiceService.purchaseInvoiceRepository
                .findOne({
                where: {
                    id
                },
                relations: ['purchaseInvoiceDetail']
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const checkPurchase = await PurchaseInvoiceService.purchaseOrderService.findById(data.purchaseOrder);
        if (!checkPurchase) {
            throw new error_1.DataNotFoundError(`Purchase ${data.purchaseOrder} does not exist`);
        }
        for (let i = 0; i < data.products.length; i++) {
            const checkProduct = await PurchaseInvoiceService.productService.findById(data.products[i].productId);
            if (!checkProduct) {
                throw new error_1.DataNotFoundError(`Product ${data.products[i].productId} does not exist`);
            }
        }
        console.log(data);
        const purchaseInvoice = new entity_1.PurchaseInvoice();
        purchaseInvoice.purchaseOrder = data.purchaseOrder;
        const purchaseInvoiceSave = await await PurchaseInvoiceService.purchaseInvoiceRepository.save(purchaseInvoice);
        const purchaseInvoiceId = purchaseInvoiceSave.id;
        for (let i = 0; i < data.products.length; i++) {
            const purchaseInvoiceDetail = new entity_1.PurchaseInvoiceDetail();
            purchaseInvoiceDetail.product = data.products[i].productId;
            purchaseInvoiceDetail.purchaseInvoice = purchaseInvoiceId;
            purchaseInvoiceDetail.price = data.products[i].pricePerItem;
            await PurchaseInvoiceService.purchaseInvoiceDetailService.create(purchaseInvoiceDetail);
        }
        try {
            return purchaseInvoiceSave;
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const purchaseInvoice = await this.findById(data.id);
        if (purchaseInvoice == null) {
            throw new error_1.DataNotFoundError(`Purchase invoice not found: ${data.id}`);
        }
        if (data.purchaseOrder !== null || data.purchaseOrder !== undefined) {
            purchaseInvoice.purchaseOrder = data.purchaseOrder;
        }
        return await PurchaseInvoiceService.purchaseInvoiceRepository.save(purchaseInvoice);
    }
    async delete(code) {
        const purchaseInvoice = await this.findById(code);
        if (purchaseInvoice == null) {
            throw new error_1.DataNotFoundError(`Purchase Invoice not found: ${code}`);
        }
        await PurchaseInvoiceService.purchaseInvoiceRepository.delete(purchaseInvoice);
        return purchaseInvoice;
    }
}
exports.PurchaseInvoiceService = PurchaseInvoiceService;
PurchaseInvoiceService.purchaseInvoiceRepository = null;
PurchaseInvoiceService.purchaseInvoiceDetailService = null;
PurchaseInvoiceService.productService = null;
PurchaseInvoiceService.purchaseOrderService = null;
//# sourceMappingURL=purchaseInvoiceService.js.map