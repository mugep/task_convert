"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SalesInvoiceService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const productService_1 = require("./productService");
const salesOrderService_1 = require("./salesOrderService");
const salesInvoiceDetailService_1 = require("./salesInvoiceDetailService");
class SalesInvoiceService {
    constructor(di) {
        if (SalesInvoiceService.salesInvoiceRepository === null) {
            SalesInvoiceService.salesInvoiceRepository = di.appTypeOrm.getConnection().getRepository(entity_1.SalesInvoice);
        }
        if (SalesInvoiceService.salesOrderService === null) {
            SalesInvoiceService.salesOrderService = new salesOrderService_1.SalesOrderService(di);
        }
        if (SalesInvoiceService.salesInvoiceDetailService === null) {
            SalesInvoiceService.salesInvoiceDetailService = new salesInvoiceDetailService_1.SalesInvoiceDetailService(di);
        }
        if (SalesInvoiceService.productService === null) {
            SalesInvoiceService.productService = new productService_1.ProductService(di);
        }
    }
    async findAll(query) {
        return await SalesInvoiceService.salesInvoiceRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['salesOrder']
        }));
    }
    async findById(id) {
        try {
            return await SalesInvoiceService.salesInvoiceRepository
                .findOne({
                where: {
                    id
                },
                relations: ['salesInvoiceDetail']
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const checkSales = await SalesInvoiceService.salesOrderService.findById(data.salesOrder);
        if (!checkSales) {
            throw new error_1.DataNotFoundError(`Sales ${data.salesOrder} does not exist`);
        }
        for (let i = 0; i < data.products.length; i++) {
            const checkProduct = await SalesInvoiceService.productService.findById(data.products[i].productId);
            if (!checkProduct) {
                throw new error_1.DataNotFoundError(`Product ${data.products[i].productId} does not exist`);
            }
        }
        console.log(data);
        const salesInvoice = new entity_1.SalesInvoice();
        salesInvoice.salesOrder = data.salesOrder;
        const salesInvoiceSave = await SalesInvoiceService.salesInvoiceRepository.save(salesInvoice);
        const salesInvoiceId = salesInvoiceSave.id;
        for (let i = 0; i < data.products.length; i++) {
            const salesInvoiceDetail = new entity_1.SalesInvoiceDetail();
            salesInvoiceDetail.product = data.products[i].productId;
            salesInvoiceDetail.salesInvoice = salesInvoiceId;
            salesInvoiceDetail.price = data.products[i].pricePerItem;
            await SalesInvoiceService.salesInvoiceDetailService.create(salesInvoiceDetail);
        }
        try {
            return salesInvoiceSave;
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const salesInvoice = await this.findById(data.id);
        if (salesInvoice == null) {
            throw new error_1.DataNotFoundError(`Sales invoice not found: ${data.id}`);
        }
        if (data.salesOrder !== null || data.salesOrder !== undefined) {
            salesInvoice.salesOrder = data.salesOrder;
        }
        return await SalesInvoiceService.salesInvoiceRepository.save(salesInvoice);
    }
    async delete(code) {
        const salesInvoice = await this.findById(code);
        if (salesInvoice == null) {
            throw new error_1.DataNotFoundError(`Sales Invoice not found: ${code}`);
        }
        await SalesInvoiceService.salesInvoiceRepository.delete(salesInvoice);
        return salesInvoice;
    }
}
exports.SalesInvoiceService = SalesInvoiceService;
SalesInvoiceService.salesInvoiceRepository = null;
SalesInvoiceService.salesInvoiceDetailService = null;
SalesInvoiceService.productService = null;
SalesInvoiceService.salesOrderService = null;
//# sourceMappingURL=salesInvoiceService.js.map