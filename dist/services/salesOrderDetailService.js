"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SalesOrderDetailService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const productService_1 = require("./productService");
class SalesOrderDetailService {
    constructor(di) {
        if (SalesOrderDetailService.salesOrderDetailRepository === null) {
            SalesOrderDetailService.salesOrderDetailRepository = di.appTypeOrm.getConnection().getRepository(entity_1.SalesOrderDetail);
        }
        if (SalesOrderDetailService.productService === null) {
            SalesOrderDetailService.productService = new productService_1.ProductService(di);
        }
        console.log(SalesOrderDetailService.productService);
        console.log(di.productService);
    }
    async findAll(query) {
        return await SalesOrderDetailService.salesOrderDetailRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['product', 'salesOrder']
        }));
    }
    async findById(id) {
        try {
            return await SalesOrderDetailService.salesOrderDetailRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const salesOrderDetail = new entity_1.SalesOrderDetail();
        salesOrderDetail.product = data.product;
        salesOrderDetail.quantity = data.quantity;
        salesOrderDetail.salesOrder = data.salesOrder;
        // await SalesOrderDetailService.productService.adjustStock(data.id, data.quantity, 'SALES_ORDER');
        const saveSalesOrderDetail = await SalesOrderDetailService.salesOrderDetailRepository.save(salesOrderDetail);
        // await PurchaseOrderService.productService.adjustStock(salesOrderDetail.product, -salesOrderDetail.quantity);
        await SalesOrderDetailService.productService.adjustStock(saveSalesOrderDetail.product, -saveSalesOrderDetail.quantity);
        try {
            return saveSalesOrderDetail;
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const salesOrderDetail = await this.findById(data.id);
        if (salesOrderDetail == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${data.id}`);
        }
        if (data.product !== null || data.product !== undefined) {
            salesOrderDetail.product = data.product;
        }
        if (data.quantity !== null || data.quantity !== undefined) {
            salesOrderDetail.quantity = data.quantity;
        }
        if (data.salesOrder !== null || data.salesOrder !== undefined) {
            salesOrderDetail.salesOrder = data.salesOrder;
        }
        return await SalesOrderDetailService.salesOrderDetailRepository.save(salesOrderDetail);
    }
    async delete(code) {
        const salesOrderDetail = await this.findById(code);
        if (salesOrderDetail == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        await SalesOrderDetailService.salesOrderDetailRepository.delete(salesOrderDetail);
        return salesOrderDetail;
    }
}
exports.SalesOrderDetailService = SalesOrderDetailService;
SalesOrderDetailService.salesOrderDetailRepository = null;
SalesOrderDetailService.productService = null;
//# sourceMappingURL=salesOrderDetailService.js.map