"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscribeToSJKService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class SubscribeToSJKService {
    constructor(di) {
        if (SubscribeToSJKService.subscribeToSjkRepository === null) {
            SubscribeToSJKService.subscribeToSjkRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Sjk);
        }
    }
    async findAll(query) {
        return await SubscribeToSJKService.subscribeToSjkRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await SubscribeToSJKService.subscribeToSjkRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const subcribeToSJK = new entity_1.Sjk();
        subcribeToSJK.name = data.name;
        subcribeToSJK.email = data.email;
        subcribeToSJK.sjkno = data.sjkno;
        subcribeToSJK.base = data.base;
        subcribeToSJK.issuedDate = data.issuedDate;
        try {
            return await SubscribeToSJKService.subscribeToSjkRepository.save(subcribeToSJK);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const subcribeToSJK = await this.findById(data.id);
        if (subcribeToSJK == null) {
            throw new error_1.DataNotFoundError(`SJK not found: ${data.id}`);
        }
        if (data.name !== null || data.name !== undefined) {
            subcribeToSJK.name = data.name;
        }
        return await SubscribeToSJKService.subscribeToSjkRepository.save(subcribeToSJK);
    }
    async delete(code) {
        const subcribeToSJK = await this.findById(code);
        if (subcribeToSJK == null) {
            throw new error_1.DataNotFoundError(`SJk not found: ${code}`);
        }
        await SubscribeToSJKService.subscribeToSjkRepository.delete(subcribeToSJK);
        return subcribeToSJK;
    }
}
exports.SubscribeToSJKService = SubscribeToSJKService;
SubscribeToSJKService.subscribeToSjkRepository = null;
//# sourceMappingURL=subscribeSjkService.js.map