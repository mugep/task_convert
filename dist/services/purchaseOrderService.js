"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseOrderService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const purchaseOrderDetailService_1 = require("./purchaseOrderDetailService");
const productService_1 = require("./productService");
const supplierService_1 = require("./supplierService");
class PurchaseOrderService {
    constructor(di) {
        if (PurchaseOrderService.purchaseOrderRepository === null) {
            PurchaseOrderService.purchaseOrderRepository = di.appTypeOrm.getConnection().getRepository(entity_1.PurchaseOrder);
        }
        if (PurchaseOrderService.purchaseOrderDetailService === null) {
            PurchaseOrderService.purchaseOrderDetailService = new purchaseOrderDetailService_1.PurchaseOrderDetailService(di);
        }
        if (PurchaseOrderService.supplierService === null) {
            PurchaseOrderService.supplierService = new supplierService_1.SupplierService(di);
        }
        if (PurchaseOrderService.productService === null) {
            PurchaseOrderService.productService = new productService_1.ProductService(di);
        }
    }
    async findAll(query) {
        return await PurchaseOrderService.purchaseOrderRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['supplier']
        }));
    }
    async findById(id) {
        try {
            return await PurchaseOrderService.purchaseOrderRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const checkSupplier = await PurchaseOrderService.supplierService.findById(data.supplier);
        if (!checkSupplier) {
            throw new error_1.DataNotFoundError(`Supplier ${data.supplier} does not exist`);
        }
        for (let i = 0; i < data.products.length; i++) {
            const checkProduct = await PurchaseOrderService.productService.findById(data.products[i].productId);
            if (!checkProduct) {
                throw new error_1.DataNotFoundError(`Product ${data.products[i].productId} does not exist`);
            }
        }
        const purchaseOrder = new entity_1.PurchaseOrder();
        purchaseOrder.supplier = data.supplier;
        purchaseOrder.date = data.date;
        const purchaseOrderSave = await PurchaseOrderService.purchaseOrderRepository.save(purchaseOrder);
        const purchaseOrderId = purchaseOrderSave.id;
        for (let i = 0; i < data.products.length; i++) {
            const purchaseOrderDetail = new entity_1.PurchaseOrderDetail();
            purchaseOrderDetail.product = data.products[i].productId;
            purchaseOrderDetail.purchaseOrder = purchaseOrderId;
            purchaseOrderDetail.quantity = data.products[i].quantity;
            await PurchaseOrderService.purchaseOrderDetailService.create(purchaseOrderDetail);
            // await PurchaseOrderService.productService.adjustStock(data.products[i].productId, data.products[i].quantity, 'PURCHASE_ORDER');
        }
        try {
            return purchaseOrderSave;
            // const purchaseOrderDetail = await PurchaseOrderService.purchaseOrderDetailService.create()
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const purchaseOrder = await this.findById(data.id);
        if (purchaseOrder == null) {
            throw new error_1.DataNotFoundError(`Purchase Order not found: ${data.id}`);
        }
        if (data.supplier !== null || data.supplier !== undefined) {
            purchaseOrder.supplier = data.supplier;
        }
        return await PurchaseOrderService.purchaseOrderRepository.save(purchaseOrder);
    }
    async delete(code) {
        const purchaseOrder = await this.findById(code);
        if (purchaseOrder == null) {
            throw new error_1.DataNotFoundError(`Purchase Order not found: ${code}`);
        }
        await PurchaseOrderService.purchaseOrderRepository.delete(purchaseOrder);
        return purchaseOrder;
    }
}
exports.PurchaseOrderService = PurchaseOrderService;
PurchaseOrderService.purchaseOrderRepository = null;
PurchaseOrderService.purchaseOrderDetailService = null;
PurchaseOrderService.productService = null;
PurchaseOrderService.supplierService = null;
//# sourceMappingURL=purchaseOrderService.js.map