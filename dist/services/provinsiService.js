"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProvinsiService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class ProvinsiService {
    constructor(di) {
        if (ProvinsiService.provinsiRepository === null) {
            ProvinsiService.provinsiRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Provinsi);
        }
    }
    async findAll(query) {
        return await ProvinsiService.provinsiRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['negara']
        }));
    }
    async findById(id) {
        try {
            return await ProvinsiService.provinsiRepository
                .findOne({
                where: {
                    id
                },
                relations: ['negara']
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const provinsi = new entity_1.Provinsi();
        provinsi.name = data.name;
        provinsi.negara = data.negara;
        try {
            return await ProvinsiService.provinsiRepository.save(provinsi);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(newProvinsi) {
        const provinsi = await this.findById(newProvinsi.id);
        if (provinsi == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${newProvinsi.id}`);
        }
        if (newProvinsi.name !== null || newProvinsi.name !== undefined) {
            provinsi.name = newProvinsi.name;
        }
        if (newProvinsi.negara !== null || newProvinsi.negara !== undefined) {
            provinsi.negara = newProvinsi.negara;
        }
        return await ProvinsiService.provinsiRepository.save(provinsi);
    }
    async delete(code) {
        const provinsi = await this.findById(code);
        if (provinsi == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        await ProvinsiService.provinsiRepository.delete(provinsi);
        return provinsi;
    }
}
exports.ProvinsiService = ProvinsiService;
ProvinsiService.provinsiRepository = null;
//# sourceMappingURL=provinsiService.js.map