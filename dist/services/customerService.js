"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class CustomerService {
    constructor(di) {
        if (CustomerService.customerRepository === null) {
            CustomerService.customerRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Customer);
        }
    }
    async findAll(query) {
        return await CustomerService.customerRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await CustomerService.customerRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const customer = new entity_1.Customer();
        customer.name = data.name;
        try {
            return await CustomerService.customerRepository.save(customer);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const customer = await this.findById(data.id);
        if (customer == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${data.id}`);
        }
        if (data.name !== null || data.name !== undefined) {
            customer.name = data.name;
        }
        return await CustomerService.customerRepository.save(customer);
    }
    async delete(code) {
        const customer = await this.findById(code);
        if (customer == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        await CustomerService.customerRepository.delete(customer);
        return customer;
    }
}
exports.CustomerService = CustomerService;
CustomerService.customerRepository = null;
//# sourceMappingURL=customerService.js.map