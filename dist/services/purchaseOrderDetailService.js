"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseOrderDetailService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const productService_1 = require("./productService");
class PurchaseOrderDetailService {
    constructor(di) {
        if (PurchaseOrderDetailService.purchaseOrderDetailRepository === null) {
            PurchaseOrderDetailService.purchaseOrderDetailRepository = di.appTypeOrm.getConnection().getRepository(entity_1.PurchaseOrderDetail);
        }
        if (PurchaseOrderDetailService.productService === null) {
            PurchaseOrderDetailService.productService = new productService_1.ProductService(di);
        }
    }
    async findAll(query) {
        return await PurchaseOrderDetailService.purchaseOrderDetailRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['purchaseOrder', 'product']
        }));
    }
    async findById(id) {
        try {
            return await PurchaseOrderDetailService.purchaseOrderDetailRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        console.log(data);
        // for (let i = 0; i < data.products.length; i++) {
        //     const salesOrderDetail = new SalesOrderDetail();
        //     salesOrderDetail.product = data.products[i].productId;
        //     salesOrderDetail.salesOrder = salesOrderId;
        //     salesOrderDetail.quantity = data.products[i].quantity;
        //     await SalesOrderService.salesOrderDetailService.create(salesOrderDetail);
        //     // await SalesOrderService.productService.adjustStock(data.products[i].productId, - data.products[i].quantity);
        // }
        const purchaseOrderDetail = new entity_1.PurchaseOrderDetail();
        purchaseOrderDetail.product = data.product;
        purchaseOrderDetail.purchaseOrder = data.purchaseOrder;
        purchaseOrderDetail.quantity = data.quantity;
        const savePurchaseOrderDetail = await PurchaseOrderDetailService.purchaseOrderDetailRepository.save(purchaseOrderDetail);
        console.log(savePurchaseOrderDetail.quantity);
        await PurchaseOrderDetailService.productService.adjustStock(savePurchaseOrderDetail.product, +savePurchaseOrderDetail.quantity);
        try {
            return savePurchaseOrderDetail;
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const purchaseOrderDetail = await this.findById(data.id);
        if (purchaseOrderDetail == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${data.id}`);
        }
        if (data.quantity !== null || data.quantity !== undefined) {
            purchaseOrderDetail.quantity = data.quantity;
        }
        return await PurchaseOrderDetailService.purchaseOrderDetailRepository.save(purchaseOrderDetail);
    }
    async delete(code) {
        const purchaseOrderDetail = await this.findById(code);
        if (purchaseOrderDetail == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        await PurchaseOrderDetailService.purchaseOrderDetailRepository.delete(purchaseOrderDetail);
        return purchaseOrderDetail;
    }
}
exports.PurchaseOrderDetailService = PurchaseOrderDetailService;
PurchaseOrderDetailService.purchaseOrderDetailRepository = null;
PurchaseOrderDetailService.productService = null;
//# sourceMappingURL=purchaseOrderDetailService.js.map