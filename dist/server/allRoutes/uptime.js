"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uptime = void 0;
exports.uptime = async (ctx) => {
    ctx.body = {
        uptime: process.uptime(),
    };
};
//# sourceMappingURL=uptime.js.map