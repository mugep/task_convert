"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.baseRoutes = exports.baseOpenApiRoutes = void 0;
// ROUTE CONTROLER IMPORTS
// GET ROUTES
// POST ROUTES
const postSubscribeSjk_1 = require("./post/postSubscribeSjk");
const postValidate_1 = require("./post/postValidate");
// REGISTER THE ROUTE CONTROLLER HERE
const routes = [
    // GET ROUTES
    // // POST ROUTES
    [postSubscribeSjk_1.openapiPostSubscribeSjk, postSubscribeSjk_1.postSubscribeSjk],
    [postValidate_1.openapiPostValidate, postValidate_1.postValidate]
    // // PUT ROUTES
    // DELETE ROUTES
];
const tmpBaseOpenApiRoutes = [];
const tmpBaseRoutes = [];
for (const route of routes) {
    if (!route[1].path.startsWith("/v1/") || !route[0].path.startsWith("/v1/")) {
        // tslint:disable-next-line: no-console
        console.log(route[1]);
        throw new Error("path should start with v1");
    }
    tmpBaseOpenApiRoutes.push(route[0]);
    tmpBaseRoutes.push(route[1]);
}
// seperate the route to openapi and application route, future easier to extend
exports.baseOpenApiRoutes = tmpBaseOpenApiRoutes;
exports.baseRoutes = tmpBaseRoutes;
//# sourceMappingURL=index.js.map