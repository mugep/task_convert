"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postValidate = exports.openapiPostValidate = void 0;
const path = "/v1/validate";
const method = "POST";
const get = [];
// const body: BodyFormat = {
//     required: true,
//     type: "object",
//     swagger: {
//         description: "",
//         example: {
//             name: "",
//             email: "",
//             sjkNo: "",
//             base: "",
//             issuedDate: ""
//         },
//     },
//     fields: {
//     },
// };
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {},
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    ctx.status = 200;
    ctx.body = "OK";
};
exports.openapiPostValidate = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postValidate = {
    func,
    method,
    path,
};
//# sourceMappingURL=postValidate.js.map