"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postSubscribeSjk = exports.openapiPostSubscribeSjk = void 0;
const path = "/v1/sjk/subscribe";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            name: "",
            email: "",
            sjkNo: "",
            base: "",
            issuedDate: ""
        },
    },
    fields: {
        name: {
            type: "string",
            required: true
        },
        email: {
            type: "string",
            required: true
        },
        sjkno: {
            type: "number",
            required: false
        },
        sidno: {
            type: "number",
            required: false
        },
        base: {
            type: "string",
            required: false
        },
        issuedDate: {
            type: "string",
            required: true
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            name: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    console.log('tipe', requestBody);
    if (requestBody.sjkno) {
        ctx.body = await di.subscribeSjkService.create(requestBody);
    }
    else if (requestBody.sidno) {
        ctx.body = await di.subscribeSidService.create(requestBody);
    }
    console.log(requestBody);
    // const requestBody = di.validator.processBody<Sjk>(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    // ctx.body = subscribeService;
};
exports.openapiPostSubscribeSjk = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postSubscribeSjk = {
    func,
    method,
    path,
};
//# sourceMappingURL=postSubscribeSjk.js.map