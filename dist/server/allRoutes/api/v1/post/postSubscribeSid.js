"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postSubscribeSid = exports.openapiPostSubscribeSid = void 0;
const path = "/v1/sid/subscribe";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            name: "",
            email: "",
            sidNo: "",
            issuedDate: ""
        },
    },
    fields: {
        name: {
            type: "string",
            required: true
        },
        email: {
            type: "string",
            required: true
        },
        sidno: {
            type: "number",
            required: true
        },
        issuedDate: {
            type: "string",
            required: true
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            name: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.subscribeSidService.create(requestBody);
};
exports.openapiPostSubscribeSid = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postSubscribeSid = {
    func,
    method,
    path,
};
//# sourceMappingURL=postSubscribeSid.js.map