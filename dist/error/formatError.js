"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FormatError = void 0;
class FormatError extends Error {
    constructor(field, formatMessage) {
        super(field + ": " + formatMessage);
        this.name = "FormatError";
    }
}
exports.FormatError = FormatError;
//# sourceMappingURL=formatError.js.map