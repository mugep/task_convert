"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DocumentNotUploadedError = void 0;
class DocumentNotUploadedError extends Error {
    constructor() {
        super("Document not uploaded");
        this.name = "DocumentNotUploadedError";
    }
}
exports.DocumentNotUploadedError = DocumentNotUploadedError;
//# sourceMappingURL=documentNotUploadedError.js.map