"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorCatcher = void 0;
const partial_responsify_1 = require("partial-responsify");
const error_1 = require("../error");
const lib_1 = require("../lib");
var ErrorCodes;
(function (ErrorCodes) {
    ErrorCodes["E0"] = "E0";
    ErrorCodes["EV"] = "EV";
    ErrorCodes["EV2"] = "EV2";
    ErrorCodes["EDOPAR"] = "EDOPAR";
    ErrorCodes["EDOPAF"] = "EDOPAF";
    ErrorCodes["EIK"] = "EIK";
    ErrorCodes["ENF"] = "ENF";
    ErrorCodes["EID"] = "EID";
    ErrorCodes["ENL"] = "ENL";
    ErrorCodes["ENL2"] = "ENL2";
    ErrorCodes["ENL3"] = "ENL3";
    ErrorCodes["ENL4"] = "ENL4";
    ErrorCodes["ENL5"] = "ENL5";
    ErrorCodes["ECPAMC"] = "ECPAMC";
    ErrorCodes["ECPAMV"] = "ECPAMV";
    ErrorCodes["EPA"] = "EPA";
    ErrorCodes["EDR"] = "EDR";
    ErrorCodes["ENA"] = "ENA";
    ErrorCodes["EMA"] = "EMA";
    ErrorCodes["EDE"] = "EDE";
    ErrorCodes["RE"] = "RE"; // RadsoftCriticalError
})(ErrorCodes || (ErrorCodes = {}));
async function errorCatcher(ctx, next) {
    try {
        await next();
    }
    catch (err) {
        // future use contentLanguage to turn ValidatorError and PartialResponsifyValidationError to multilanguage;
        let status = err.status || 500;
        const errorCode = ErrorCodes.E0;
        const body = {
            errorCode,
            message: err.message,
        };
        console.log(err);
        if (err instanceof lib_1.ValidatorError) {
            status = 400;
            body.errorCode = ErrorCodes.EV;
        }
        else if (err instanceof partial_responsify_1.PartialResponsifyValidationError) {
            status = 400;
            body.errorCode = ErrorCodes.EV2;
            console.log(err.formatErrs && err.formatErrs.map((field) => field.name));
        }
        else if (err instanceof error_1.InvalidKeyError) {
            status = 400;
            body.errorCode = ErrorCodes.EIK;
        }
        else if (err instanceof error_1.DataNotFoundError) {
            status = 400;
            body.errorCode = ErrorCodes.ENF;
        }
        else if (err instanceof error_1.NoUnitholderIdError) {
            status = 400;
            body.errorCode = ErrorCodes.ENL;
        }
        else if (err instanceof error_1.DocumentNotUploadedError) {
            status = 400;
            body.errorCode = ErrorCodes.ENL2;
        }
        else if (err instanceof error_1.DocumentNotAllowUpdateError) {
            status = 400;
            body.errorCode = ErrorCodes.ENL3;
        }
        else if (err instanceof error_1.DateRangeError) {
            status = 400;
            body.errorCode = ErrorCodes.EDR;
        }
        else if (err instanceof error_1.DuplicateEntryError) {
            status = 400;
            body.errorCode = ErrorCodes.EDE;
        }
        else if (err instanceof error_1.NotAuthorizeError) {
            status = 401;
            body.errorCode = ErrorCodes.ENA;
        }
        console.log(err);
        ctx.status = status;
        ctx.body = body;
        if (status === 500) {
            ctx.app.emit("error", err, ctx);
        }
    }
}
exports.errorCatcher = errorCatcher;
//# sourceMappingURL=errorCatcher.js.map