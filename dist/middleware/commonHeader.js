"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.commonHeader = void 0;
const scripts_1 = require("../scripts");
const headerParams = scripts_1.commonHeaderParams;
async function commonHeader(ctx, next) {
    const di = ctx.state.di;
    const headers = di.validator.processHeader(headerParams, ctx.request.headers);
    ctx.set("Cache-Control", "no-store, no-cache, must-revalidate");
    ctx.set("Pragma", "no-cache");
    ctx.set("Expires", "0");
    ctx.state.contentLanguage = headers["content-language"];
    await next();
}
exports.commonHeader = commonHeader;
//# sourceMappingURL=commonHeader.js.map