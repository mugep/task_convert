"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Provinsi = void 0;
const typeorm_1 = require("typeorm");
const kota_1 = require("./kota");
const negara_1 = require("./negara");
let Provinsi = class Provinsi {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: "id" }),
    __metadata("design:type", String)
], Provinsi.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'name' }),
    __metadata("design:type", String)
], Provinsi.prototype, "name", void 0);
__decorate([
    typeorm_1.ManyToOne(() => negara_1.Negara, negara => negara.id),
    typeorm_1.JoinColumn({ name: "negara_id" }),
    __metadata("design:type", negara_1.Negara)
], Provinsi.prototype, "negara", void 0);
__decorate([
    typeorm_1.OneToMany(() => kota_1.Kota, (kota) => kota.provinsi, { onDelete: 'CASCADE', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Provinsi.prototype, "kota", void 0);
Provinsi = __decorate([
    typeorm_1.Entity({
        name: 'provinsi'
    })
], Provinsi);
exports.Provinsi = Provinsi;
//# sourceMappingURL=provinsi.js.map