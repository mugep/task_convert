"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseInvoice = void 0;
const typeorm_1 = require("typeorm");
const purchaseInvoiceDetail_1 = require("./purchaseInvoiceDetail");
const purchaseOrder_1 = require("./purchaseOrder");
let PurchaseInvoice = class PurchaseInvoice {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: 'id' }),
    __metadata("design:type", Number)
], PurchaseInvoice.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(() => purchaseOrder_1.PurchaseOrder, purchaseOrder => purchaseOrder.id),
    typeorm_1.JoinColumn({ name: 'purchase_order_id' }),
    __metadata("design:type", Number)
], PurchaseInvoice.prototype, "purchaseOrder", void 0);
__decorate([
    typeorm_1.OneToMany(() => purchaseInvoiceDetail_1.PurchaseInvoiceDetail, purchaseInvoiceDetail => purchaseInvoiceDetail.id),
    __metadata("design:type", Array)
], PurchaseInvoice.prototype, "purchaseInvoiceDetail", void 0);
PurchaseInvoice = __decorate([
    typeorm_1.Entity({
        name: 'purchase_invoice'
    })
], PurchaseInvoice);
exports.PurchaseInvoice = PurchaseInvoice;
//# sourceMappingURL=purchaseInvoice.js.map