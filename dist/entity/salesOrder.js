"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SalesOrder = void 0;
const typeorm_1 = require("typeorm");
const customer_1 = require("./customer");
const salesOrderDetail_1 = require("./salesOrderDetail");
let SalesOrder = class SalesOrder {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: 'id' }),
    __metadata("design:type", Number)
], SalesOrder.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'date' }),
    __metadata("design:type", Date)
], SalesOrder.prototype, "date", void 0);
__decorate([
    typeorm_1.ManyToOne(() => customer_1.Customer, customer => customer.id),
    typeorm_1.JoinColumn({ name: 'customer_id' }),
    __metadata("design:type", customer_1.Customer)
], SalesOrder.prototype, "customer", void 0);
__decorate([
    typeorm_1.OneToMany(() => salesOrderDetail_1.SalesOrderDetail, salesOrderDetail => salesOrderDetail.id),
    __metadata("design:type", Array)
], SalesOrder.prototype, "salesOrderDetail", void 0);
SalesOrder = __decorate([
    typeorm_1.Entity({
        name: 'sales_order'
    })
], SalesOrder);
exports.SalesOrder = SalesOrder;
//# sourceMappingURL=salesOrder.js.map